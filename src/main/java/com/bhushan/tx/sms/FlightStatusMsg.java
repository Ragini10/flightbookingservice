package com.bhushan.tx.sms;

import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.stereotype.Service;

import com.bhushan.tx.dto.FlightBookingAcknowledgement;

@Service
public class FlightStatusMsg {
	public static HttpResponse sendMsg(FlightBookingAcknowledgement flightBookingAcknowledgement)
			throws ClientProtocolException, IOException {

		String authKey = "L1SypZi6xXz5tvgk4Ab7VIheqK9GjYcTC83BQNF0Ea2flnDPmO4TiKthmdeps5IZUC6NqxBP7F8MJcRV";
		String msg = "Hi_dude_your_Ticket_has_been_booked_having_pnr" + "...." + flightBookingAcknowledgement.getPnrNo()
				+ "...." + "check_your_mail" + "...." + flightBookingAcknowledgement.getPassengerInfo().getEmail()
				+ "...." + "for_more_such_details" + "" + flightBookingAcknowledgement.getStatus()
				+ "Looking_forward_to_serve_you_again";
		HttpClient client = HttpClientBuilder.create().build();
		String url = "https://www.fast2sms.com/dev/bulk?authorization=" + authKey + "&sender_id=FSTSMS&message=" + msg
				+ "&language=english&route=p&numbers=" + flightBookingAcknowledgement.getPassengerInfo().getMobileno();
		HttpUriRequest httpUriRequest = new HttpGet(url);
		HttpResponse response = client.execute(httpUriRequest);
		return response;

	}
}